The goal of the project is to find bug reports for (our tools)[https://gitlab.com/umb-svl/faial/].

Keywords of interest:
- bank conflict
- data-race
- uncoalesced accesses

The search should be confined to CUDA kernels or WGSL kernels.

A problem with searching in GitHub is that you'll find many duplicates.

You can search commit messages, comments in the source code, or issues.

